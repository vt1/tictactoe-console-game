#include "Cell.h"
#include "Game.h"
#include "Field.h"
#include <iostream>
#include <string>
using namespace std;


void Cell::initCell(Cell *pCell)
{
	for(int i = 0; i < 3; i++)
		for(int j = 0; j < 3; j++)
			pCell->isEmpty[i][j] = true;
	
	pCell->pNameCell[0][0] = "A1";
	pCell->pNameCell[0][1] = "B1";
	pCell->pNameCell[0][2] = "C1";
	pCell->pNameCell[1][0] = "A2";
	pCell->pNameCell[1][1] = "B2";
	pCell->pNameCell[1][2] = "C2";
	pCell->pNameCell[2][0] = "A3";
	pCell->pNameCell[2][1] = "B3";
	pCell->pNameCell[2][2] = "C3";

	for(int g = 0; g < 3; g++)
		for(int h = 0; h < 3; h++)
			pCell->cellValue[g][h] = "_";
}

Cell::Cell()
{	
	initCell(this);
}

void Cell::updateCell() // �������� ���� �� createField();
{
	//createField();
	system("cls");
	char tmpVarForFilling;
	cout << "_" << " ";
	for(int i = 65; i < 68; i++)
	{
		tmpVarForFilling = i;
		cout << tmpVarForFilling << " ";		
	}	
	cout << endl;
	cout << 1 << " " << this->cellValue[0][0] << " " << this->cellValue[0][1] << " " << this->cellValue[0][2] << '\n';			
	cout << 2 << " " << this->cellValue[1][0] << " " << this->cellValue[1][1] << " " << this->cellValue[1][2] << '\n';	
	cout << 3 << " " << this->cellValue[2][0] << " " << this->cellValue[2][1] << " " << this->cellValue[2][2] << '\n';		
}


bool Cell::acrossingCell(bool isContinue) 
{
	if(this->XorO[0][0] + this->XorO[0][1] + this->XorO[0][2] == 9 ||	//vertical X;
		this->XorO[1][0] + this->XorO[1][1] + this->XorO[1][2] == 9 ||
		this->XorO[2][0] + this->XorO[2][1] + this->XorO[2][2] == 9 ||

		this->XorO[0][0] + this->XorO[1][0] + this->XorO[2][0] == 9 || // horizontal X;
		this->XorO[0][1] + this->XorO[1][1] + this->XorO[2][1] == 9 ||
		this->XorO[0][2] + this->XorO[1][2] + this->XorO[2][2] == 9 ||
		
		this->XorO[0][0] + this->XorO[1][1] + this->XorO[2][2] == 9 ||  //diagonal X;
		this->XorO[2][0] + this->XorO[1][1] + this->XorO[0][2] == 9)		
	{
		cout << "Game Over" << endl;
		cout << "X winsssssssssssss" << endl;	
		isContinue = false;		
	}

	if(this->XorO[0][0] + this->XorO[0][1] + this->XorO[0][2] == 12 ||	//vertical O;
		this->XorO[1][0] + this->XorO[1][1] + this->XorO[1][2] == 12 ||
		this->XorO[2][0] + this->XorO[2][1] + this->XorO[2][2] == 12 ||

		this->XorO[0][0] + this->XorO[1][0] + this->XorO[2][0] == 12 || // horizontal O;
		this->XorO[0][1] + this->XorO[1][1] + this->XorO[2][1] == 12 ||
		this->XorO[0][2] + this->XorO[1][2] + this->XorO[2][2] == 12 ||
		
		this->XorO[0][0] + this->XorO[1][1] + this->XorO[2][2] == 12 ||  //diagonal O;
		this->XorO[2][0] + this->XorO[1][1] + this->XorO[0][2] == 12)
	{
		cout << "Game Over" << endl;
		cout << "O winssssssssssssss" << endl;	
		isContinue = false;
	}

	if(this->nobodyWins[0][0] + this->nobodyWins[0][1] + this->nobodyWins[0][2] +	//if nobody wins;
		this->nobodyWins[1][0] + this->nobodyWins[1][1] + this->nobodyWins[1][2] +
		this->nobodyWins[2][0] + this->nobodyWins[2][1] + this->nobodyWins[2][2] == 45)	
	{
		if(isContinue == true)
		cout << "Game Over" << endl;
		cout << "Nobody won" << endl;
		isContinue = false;
	}
	return isContinue;
}