class Cell
{
public:
	void initCell(Cell *pCell);
	Cell();
	void updateCell();
	bool acrossingCell(bool isContinue);
	
	char *pNameCell[3][3];
	bool isEmpty[3][3];
	int XorO[3][3];
	char *cellValue[3][3];
	int nobodyWins[3][3];
};