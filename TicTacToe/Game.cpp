#include "Game.h"
#include "Field.h"
#include "Cell.h"
#include <iostream>
#include <string>
using namespace std;


Game::Game()
{	
	//
}

void Game::createMenu()
{
	system("cls");
	cout << "1.Start game" << endl;
	cout << "2.How to Play" << endl;
	cout << "3.Quit" << endl;	
	cout << "Choose: ";
	int cursor;
	cin >> cursor;
	
	switch(cursor)
	{
	case 1:	
		Field field;
		field.createField();
		break;
	case 2:
		system("cls");
		cout << "Entering name cells. Example: A1,C3" << endl;
		cout << "1.Back to Menu" << endl;
		cin >> cursor;
		if(cursor == 1)
			createMenu();
		break;
	case 3:
		break;
	default:
		break;
	}	
}

void Game::move()
{
	Cell *pCell = new Cell();	
	isContinue = true;
	int cntWhoisGoing = 1; // 1,3,5,7,9,11 = goes X; 2,4,6,8,10 = goes O;

	while(isContinue)
	{	
		string enteredNameValue;	
		cout << "Enter: ";
		cin >> enteredNameValue;	
		
		for(int i = 0; i < 3; i++)
		{		
			for(int j = 0; j < 3; j++)
			{								
				string defaultNameValue = string(pCell->pNameCell[i][j]);
				if(defaultNameValue == enteredNameValue)
				{									
					if(cntWhoisGoing % 2 != 0) 
					{					
						if(pCell->isEmpty[i][j])
						{											
							pCell->cellValue[i][j] = "X";
							pCell->isEmpty[i][j] = false;
							pCell->XorO[i][j] = 3;	
							pCell->nobodyWins[i][j] = 5;
							defaultNameValue = "";
							cntWhoisGoing++;
							pCell->updateCell();		
							isContinue = pCell->acrossingCell(isContinue);
							if(!isContinue)
							{		
								cout << "1. Back to Menu: " << endl;								
								int tmpVar;
								cin >> tmpVar;
								if(tmpVar == 1)
									createMenu();	
							}
						}	
						else
						{
							cout << "This cell is already empty" << endl;
						}
					}
					else
					{
						if(pCell->isEmpty[i][j])
						{						
							pCell->cellValue[i][j] = "O";
							pCell->isEmpty[i][j] = false;
							pCell->XorO[i][j] = 4;	
							pCell->nobodyWins[i][j] = 5;
							defaultNameValue = "";
							cntWhoisGoing++;
							pCell->updateCell();
							isContinue = pCell->acrossingCell(isContinue);
							if(!isContinue)
							{		
								cout << "1. Back to Menu: " << endl;								
								int tmpVar;
								cin >> tmpVar;
								if(tmpVar == 1)
									createMenu();	
							}
						}
						else
						{
							cout << "This cell is already empty" << endl;
						}
					}
				}
			}
		}
	}				
}					









